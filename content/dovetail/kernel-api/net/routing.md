---
title: "IP Routing"
weight: 3
---

When it comes to routing IP packets, an out-of-band network stack has
two options:

- implement its own, private routing tables which may be used from the
  out-of-band context in order to determine which device should be
  passed egress packets for transmission to any given IP
  destination. This approach does not require any synchronization
  between the in-band and out-of-band network stacks, but requires to
  maintain a redundant routing system side-by-side with the one
  already available from the in-band stack (i.e. in-kernel tables,
  decision making logic, and specific admin tools to manage the
  tables).
  
OR,

- reuse the infrastructure already available from the in-band network
  stack, specifically the outcome of its routing decisions. This
  approach requires that the out-of-band stack should be informed of
  any routing decision of interest the in-band stack may take, so that
  it could record it for later use without requiring any further
  synchronization. In addition, the common admin tools can be used to
  maintain the routing information associated to the out-of-band
  traffic.

Dovetail favors the latter way of implementing a packet routing system
in a companion core. To this end, it provides a dedicated hook which
informs such core about the outcome of any routing decision taken by
the regular in-band network stack, which involves a device acting as
an [out-of-band network port]({{< relref
"dovetail/kernel-api/net/devices#dovetail-net-io" >}}). In other
words, each time the in-band routing logic determines that some packet
should be handed to a particular network device which also handles
out-of-band traffic, the companion core is passed the details of the
routing decision, specifically the ( destination IP, destination
device ) pair.

{{% notice info %}}
Dovetail provides no facility to reuse the packet management
infrastructure such as network filtering, NAT or traffic shapping
implemented by the in-band stack, from the out-of-band stage. If
required to operate from the out-of-band stage, an implementation
of such features should be provided locally by the companion core.
{{% /notice %}}

---

### Routing-related hooks

The Dovetail interface shares the packet routing decision it takes
with a companion core by calling the following weakly bound routine
which the latter can implement.

{{< proto ip_learn_oob_route >}}
void ip_learn_oob_route(struct net *net, struct flowi4 *flp4, struct rtable *rt)
{{< /proto >}}

This hook is called each time the IPv4 routing logic from the in-band
network stack determines that some packet should be handed to a
particular network device which also handles out-of-band traffic. A
typical implementation of such hook in a companion core would record a
private copy of the received information which could be retrieved from
the out-of-band stage later on, without sharing any lock with the
in-band stack. See the EVL implementation.

{{% argument net %}}
The network namespace this decision applies to.
{{% /argument %}}

{{% argument flp4 %}}
The request being resolved by the routing decision. Specifically, the
( `rt->dst.dev`, `flp4->daddr` ) pair tells us which network device
should be passed an outgoing packet we want to send to a particular
destination referred to by its IPv4 address.
{{% /argument %}}

{{% argument rt %}}
The routing table information which represents the routing decision.
{{% /argument %}}

This call always runs in-band, possibly on softirq context.

---
