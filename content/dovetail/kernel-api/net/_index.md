---
title: "Networking"
weight: 121
pre: "&#9702; "
---

Out-of-band networking means the ability for the tasks managed by a
companion core to send and receive network packets from the
[out-of-band Dovetail stage]({{< relref
"dovetail/kernel-api/#two-stage-pipeline" >}}), i.e. guaranteeing
ultra-low latency. Dovetail brings in the necessary hooks and features
into the Linux network stack in order for a companion core to
implement its own. This section details two aspects of the support
Dovetail provides for out-of-band networking:

- the kernel API available to NIC driver writers for enabling a
  complete [out-of-band I/O path]({{< relref "#dovetail-net-full-oob"
  >}}) between applications and the network interface controller.
  
- the kernel hooks into the in-band network stack a companion core can
  rely on in order to implement its own (out-of-band) stack, such as
  the [EVL network stack]({{< relref "core/net/" >}}). Those hooks
  allow the companion core to interpose on key events related to
  networking in the Linux kernel.  They live in several areas of the
  Linux network stack, namely the [buffer management]({{< relref
  "dovetail/kernel-api/net/buffers" >}}), [socket interface]({{< relref
  "dovetail/kernel-api/net/sockets" >}}) and [device handling]({{< relref
  "dovetail/kernel-api/net/devices" >}}).

### Input diversion and out-of-band network ports {#dovetail-net-io}

At the device level, out-of-band networking requires:

- the ability to redirect the input/ingress data flow received from
  NIC drivers to the companion core so that it may pick the packets
  which should be delivered to out-of-band tasks, leaving the rest to
  normal handling by the in-band/regular network stack. This is what
  Dovetail calls _input diversion_, this is a per-device (aka
  _netdev_) property which can be turned on or off dynamically. Which
  user-level interface should be used to perform that switch is
  defined by the companion core, not Dovetail. Likewise, the companion
  core implements the mechanism for selecting the packets which
  Dovetail submits via the [netif_deliver_oob() hook]({{< relref
  "dovetail/kernel-api/net/devices#netif_deliver_oob" >}}).
  
- the ability to define network devices as I/O endpoints for
  applications to receive and send packets from the out-of-band
  stage. Dovetail calls such devices _out-of-band network ports_.
  
These are separate properties. A **virtual** network device such as a
VLAN device could me made to act as an out-of-band port for
applications, but would still need the underlying **real** network
device it sits on to divert its input to the companion
core. Conversely, a real network device can divert input and _could_
be made an out-of-band port as well, but that is not a
requirement. The rules for accepting packets from the diverted input
flow in the out-of-band network stack are exclusively defined by the
companion core, which may apply whatever filter it sees
fit. Typically, the EVL network stack can use either [VLAN tagging or
eBPF filtering]({{< relref "core/net/#evl-input-diversion" >}}) for
that purpose.

### Full or partial support for out-of-band networking {#dovetail-net-full-oob}

Dovetail does not _require_ NIC drivers to enable the [out-of-band
operating mode]({{< relref "dovetail/kernel-api/net/devices" >}}). Indeed, one
could use an unmodified stock driver with an out-of-band network
stack, at the expense of lower latency figures and bounded execution
times. In this case, the real-time I/O path would be restricted to the
execution of the network stack implemented by the companion core,
deferring the actual I/O operations to the in-band stage from which
unmodified drivers would handle them. On the other hand, one _must_
provide [out-of-band support]({{< relref "dovetail/kernel-api/net/devices" >}})
in the NIC driver in order to achieve complete, end-to-end real-time
support between the application and the transmission hardware.

---

{{<lastmodified>}}
