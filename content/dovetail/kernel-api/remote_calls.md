---
title: "Remote function calls"
weight: 70
---

{{< proto smp_call_function_oob >}}
void smp_call_function_oob(int cpu, smp_call_func_t func, void *info, bool wait)
{{< /proto >}}

This function schedules a function call on a target CPU. It is
semantically equivalent to the regular `smp_call_function_single()`
routine, except that its runs the callback on the out-of-band stage
with hard irqs off.

{{% argument cpu %}}
The target CPU number to run the callback function on, which must be
fast and non-blocking. The current CPU may issue a function call to
itself, in which case `func` is executed before this call
returns. Otherwise, the `CALL_FUNCTION_OOB_IPI` interrupt is
immediately sent to that CPU to notify it about the pending job.
{{% /argument %}}

{{% argument func %}}
The callback function which should run on the target CPU. This
callback receives the _info_ pointer as its sole argument.
{{% /argument %}}

{{% argument info %}}
An opaque pointer to pass to the function.
{{% /argument %}}

{{% argument wait %}}
If true, wait until the function has completed on the target CPU.
{{% /argument %}}

This call may be invoked either from the in-band or out-of-band stages.

---

{{<lastmodified>}}
