---
title: "Building EVL"
weight: 1
---

## Building EVL from source

{{% mixedgrid src="/images/overview-build-process.png" %}}

Building EVL from the source code is a two-step process: we need to
build a kernel enabling the EVL core, and the library implementing the
user API to this core - aka [libevl]({{< relref
"core/user-api/_index.md" >}}) - using the proper toolchain. These
steps may happen in any order. The output of this process is:

- a Linux kernel image featuring [Dovetail]({{< relref
  "dovetail/_index.md" >}}) and the [EVL core]({{< relref
  "core/_index.md" >}}) on top of it.

- the `libevl.so` shared library<sup>*</sup> which enables
  applications to request services from the EVL core, along with a few
  [basic utilities]({{< relref "core/commands.md" >}}) and [test
  programs]({{< relref "core/testing.md" >}}).

  <sup>*</sup> The static archive `libevl.a` is generated as well.
 
{{% /mixedgrid %}}

### Getting the sources

The Xenomai 4 sources are maintained in [GIT](https://git-scm.com)
repositories. As a preliminary step, you may want to have a look at
the [EVL development process]({{< relref "devprocess.md" >}}), in
order to determine which GIT branches you may be interested in these
repositories:

- The kernel tree featuring the EVL core:

  * git@git.xenomai.org:Xenomai/xenomai4/linux-evl.git
  * https://git.xenomai.org/xenomai4/linux-evl.git

- The `libevl` tree which provides the user interface to the core:

  * git@git.xenomai.org:Xenomai/xenomai4/libevl.git
  * https://git.xenomai.org/xenomai4/libevl.git

{{% notice tip %}}
Unlike its predecessor [Xenomai 3](https://v3.xenomai.org/), you do
not need any additional patch to be applied to the kernel GIT tree
mentioned above since it already contains the EVL core. Likewise,
there is no preparation script to run: the kernel tree is ready to
build from.
{{% /notice %}}

### Other prerequisites {#building-evl-prereq}

In addition to the source code, we need:

- a GCC toolchain for the target CPU architecture.

- the UAPI headers from the target Linux kernel aligned with the ABI
  requirements of `libevl.so`. Those headers export the definitions
  and interface types which `libevl.so` should use to interact with
  the EVL core from user-space, so that the former can submit
  well-formed system calls to the latter. In other words, to build
  `libevl.so`, we need access to the ABI-related files from a source
  kernel tree. Those headers may be directly available from the
  (cross-)compilation toolchain, or installed on the build host from
  some _kernel-headers_ package. If not, the build system should be
  passed the `-Duapi` option to locate those files (see the
  explanations about [configuring the build system]({{< relref
  "#libevl-build-config" >}}) for libevl).

{{% notice warning %}}
libevl relies on thread-local storage support (TLS), which might be
broken in some obsolete (ARM) toolchains. Make sure to use a current one.
{{% /notice %}}

### Building the core {#building-evl-core}

Once your favorite kernel configuration tool is brought up, you should
see the EVL configuration block somewhere inside the **General setup**
menu. This configuration block looks like this:

![Alt text](/images/core_xconfig.png "EVL core configuration")

Enabling `CONFIG_EVL` should be enough to get you started, the default
values for other EVL settings are safe to use. You should make sure to
have `CONFIG_EVL_LATMUS` and `CONFIG_EVL_HECTIC` enabled too; those
are drivers required for running the `latmus` and `hectic` utilities
available with `libevl`, which measure latency and validate the
context switching sanity.

{{% notice tip %}}
If you are unfamiliar with building kernels, [this
document](https://kernelnewbies.org/KernelBuild) may help. If you face
hurdles building directly into the kernel source tree as illustrated
in the document mentioned, you may want to check whether building
out-of-tree might work, since this is how Dovetail/EVL developers
usually rebuild kernels. If something goes wrong while building
in-tree or out-of-tree, please send a note to the [EVL mailing
list](https://subspace.kernel.org/lists.linux.dev.html) with the relevant
information.
{{% /notice %}}

#### All core configuration options {#core-kconfig}

<div>
<style>
#kconfig {
       width: 100%;
}
#kconfig th {
       text-align: center;
}
#kconfig td {
       text-align: left;
}
#kconfig tr:nth-child(even) {
       background-color: #f2f2f2;
}
#kconfig td:nth-child(2) {
       text-align: center;
}
</style>

<table id="kconfig">
  <col width="10%">
  <col width="5%">
  <col width="85%">
  <tr>
    <th>Symbol name</th>
    <th>Default</th> 
    <th>Purpose</th> 
  </tr>
  <tr>
    <td><a href="/core/" target="_blank">CONFIG_EVL</a></td>
    <td>N</td>
    <td>Enable the EVL core</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/scheduling/#SCHED_QUOTA" target="_blank">CONFIG_EVL_SCHED_QUOTA</a></td>
    <td>N</td>
    <td>Enable the quota-based scheduling policy</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/scheduling/#SCHED_TP" target="_blank">CONFIG_EVL_SCHED_TP</a></td>
    <td>N</td>
    <td>Enable the time-partitioning scheduling policy</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/scheduling/#SCHED_TP" target="_blank">CONFIG_EVL_SCHED_TP_NR_PART</a></td>
    <td>N</td>
    <td>Number of time partitions for CONFIG_EVL_SCHED_TP</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_HIGH_PERCPU_CONCURRENCY</td>
    <td>N</td>
    <td>Optimizes the implementation for applications with many real-time threads running concurrently on any given CPU	core</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/thread/#thread-stats" target="_blank">CONFIG_EVL_RUNSTATS</a></td>
    <td>Y</td>
    <td>Collect runtime statistics about threads</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_COREMEM_SIZE</td>
    <td>2048</td>
    <td>Size of the core memory heap (in kilobytes)</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/thread/" target="_blank">CONFIG_EVL_NR_THREADS</a></td>
    <td>256</td>
    <td>Maximum number of EVL threads</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_NR_MONITORS</td>
    <td>512</td>
    <td>Maximum number of EVL monitors (i.e. mutexes + semaphores + flags + events)</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/clock/" target="_blank">CONFIG_EVL_NR_CLOCKS</a></td>
    <td>8</td>
    <td>Maximum number of EVL clocks</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/xbuf/" target="_blank">CONFIG_EVL_NR_XBUFS</a></td>
    <td>16</td>
    <td>Maximum number of EVL cross-buffers</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/proxy/" target="_blank">CONFIG_EVL_NR_PROXIES</a></td>
    <td>64</td>
    <td>Maximum number of EVL proxies</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/observable/" target="_blank">CONFIG_EVL_NR_OBSERVABLES</a></td>
    <td>64</td>
    <td>Maximum number of EVL observables (does not include threads)</td>
  </tr>
  <tr>
    <td><a href="/core/runtime-settings" target="_blank">CONFIG_EVL_LATENCY_USER</a></td>
    <td>0</td>
    <td>Pre-set core timer gravity value for user threads (0 means use pre-calibrated value)</td>
  </tr>
  <tr>
    <td><a href="/core/runtime-settings" target="_blank">CONFIG_EVL_LATENCY_KERNEL</a></td>
    <td>0</td>
    <td>Pre-set core timer gravity value for kernel threads (0 means use pre-calibrated value)</td>
  </tr>
  <tr>
    <td><a href="/core/runtime-settings" target="_blank">CONFIG_EVL_LATENCY_IRQ</a></td>
    <td>0</td>
    <td>Pre-set core timer gravity value for interrupt handlers (0 means use pre-calibrated value)</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_DEBUG</td>
    <td>N</td>
    <td>Enable debug features</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_DEBUG_CORE</td>
    <td>N</td>
    <td>Enable core debug assertions</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_DEBUG_CORE</td>
    <td>N</td>
    <td>Enable core debug assertions</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_DEBUG_MEMORY</td>
    <td>N</td>
    <td>Enable debug checks in core memory allocator.
        **This option adds a significant overhead affecting latency figures**</td>
  </tr>
  <tr>
    <td><a href="/core/user-api/thread/#health-monitoring" target="_blank">CONFIG_EVL_DEBUG_WOLI</a></td>
    <td>N</td>
    <td>Enable warn-on-lock-inconsistency checkpoints</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_WATCHDOG</td>
    <td>Y</td>
    <td>Enable watchdog timer</td>
  </tr>
  <tr>
    <td>CONFIG_EVL_WATCHDOG_TIMEOUT</td>
    <td>4</td>
    <td>Watchdog timeout value (in seconds).</td>
  </tr>
  <tr>
    <td><a href="/core/drivers/gpio" target="_blank">CONFIG_GPIOLIB_OOB</a></td>
    <td>n</td>
    <td>Enable support for out-of-band GPIO line handling requests.</td>
  </tr>
  <tr>
    <td><a href="/core/drivers/spi" target="_blank">CONFIG_SPI_OOB, CONFIG_SPIDEV_OOB</a></td>
    <td>n</td>
    <td>Enable support for out-of-band SPI transfers.</td>
  </tr>
</table>

#### Enabling 32-bit support in a 64-bit kernel (`CONFIG_COMPAT`) {#enable-kernel-compat-mode}

Starting from [EVL ABI]({{< relref "core/under-the-hood/abi.md" >}})
20 in the v5.6 series, the EVL core generally allows 32-bit
applications to issue system calls to a 64-bit kernel when both the 32
and 64-bit CPU architectures are supported, such as ARM (aka Aarch32)
code running over an arm64 (Aarch64) kernel. For arm64, you need to
turn on `CONFIG_COMPAT` and `CONFIG_COMPAT_VDSO` in the kernel
configuration. To be allowed to change the latter, the
`CROSS_COMPILE_COMPAT` environment variable should be set to the
prefix of the 32-bit ARMv7 toolchain which should be used to compile
the vDSO (yes, this is quite convoluted). For instance:

```
$ make <your-make-args> ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- CROSS_COMPILE_COMPAT=arm-linux-gnueabihf- (x|g|menu)config
```

{{% notice tip %}}
For instance, if you plan to run EVL over any of the [Raspberry
PI](https://raspberrypi.org) 64-bit computers, you may find useful to
use the PI-centric 32-bit Linux distributions readily available such
as [Raspbian](https://www.raspberrypi.org/downloads/raspbian/). To do
so, make sure to enable `CONFIG_COMPAT` and `CONFIG_COMPAT_VDSO` for
your EVL-enabled kernel, building the 32-bit vDSO alongside as
mentioned earlier.
{{% /notice %}}

### Building `libevl` {#building-libevl}

#### Installing `meson`

`libevl` is built using the [meson](https://mesonbuild.com) build
system.  The [Meson project](https://mesonbuild.com/) publishes a
well-written and helpful documentation for every stage, from writing
build rules to using them. First, you need to install this
software. Since `Meson` is shipped by most Linux distributions, you
should be able to install it via the common package management for
your system. For instance, the following command should do on a
Fedora-based system:

```
$ sudo dnf install meson ninja-build
```

Since `meson` is implemented in Python3, you also have the option to
get it from the recommended Python package installer (aka `pip`), as
described in this
[document](https://mesonbuild.com/Getting-meson.html#installing-meson-with-pip).

{{% notice note %}}
Prior to `libevl` release #29, the build system was implemented as a
set of mere Makefiles. If you plan to run a legacy release, please
refer to this [document]({{< relref "core/build-libevl-legacy" >}})
instead.
{{% /notice %}}

#### Using `meson` to build `libevl`

`meson` enables a common build and installation process, based on the
usual _configure_, _compile_ and _install_ steps. These steps always
happen in a _separate_ build tree, as enforced by `meson` (in other
words, do not try building directly from the `libevl` source tree -
that won't work, besides, this would be a Bad Idea <sup>TM</sup> to do
so).

The following process is based on `meson` 0.60.1. Earlier releases may
require a slightly different syntax, but the general logic remains the
same.

##### Configuration step {#libevl-build-config}

First, we need to configure the build tree for `libevl`. The generic
syntax for this is:

```
$ meson setup [--cross-file <x-file>] [-Duapi=<kernel-uapi>] [-Dbuildtype=<build-type>] [-Dprefix=<prefix>] $buildir $srcdir
```

> Common configuration settings passed to `meson`

| Variable       |  Description
| -------------  |  -----------
| $buildir       |  Path to the build directory (separate from $srcdir)
| $srcdir        |  Path to the `libevl` source tree
| \<prefix>      |  The installation prefix (installation path is $DESTDIR/$prefix)
| \<build-type>  |  A build type, such as _debug_, _debugoptimized_, or _release_
| \<x-file>      |  A `meson` [cross-compilation file](https://mesonbuild.com/Cross-compilation.html) defining the build environment
| \<kernel-uapi> |  A path to the kernel source tree containing the [UAPI headers]({{< relref "#building-evl-prereq" >}})

A couple of pre-defined cross-compilation files is shipped with
`libevl` in the `meson/` directory at the top level of the source
hierarchy (`$srcdir/meson`), namely:

- aarch64-linux-gnu
- arm-linux-gnueabihf

A cross-file content is fairly straightforward for anyone with a first
experience using a cross-compilation toolchain, and fully documented
[there](https://mesonbuild.com/Cross-compilation.html).

> Other build settings

`meson` provides many other settings which influence the build and
installation processes. An exhaustive description is given in this
[document](https://mesonbuild.com/Running-Meson.html#configuring-the-build-directory).

##### Compilation step

Next, we run the compilation proper. The generic syntax for compiling
`libevl` from a configured build directory is:

```
$ meson compile [-v]
```

`-v` tells `meson` to run verbosely, displaying every step it takes to
complete the build.

##### Installation step

Eventually, we can install the artefacts produced by the build
process. The generic syntax for installing `libevl` after a successful
build is:

```
$ [DESTDIR=<staging-dir>] ninja install
```

All the binary artefacts produced are copied to $DESTDIR/$prefix, using
the $prefix set during the configuration step. For instance:

```
$ DESTDIR=/nfsroot/generic/armv7 ninja install 
[1/211] Generating lib/git_stamp.h with a custom command
[2/211] Compiling C object lib/libevl.so.4.0.0.p/clock.c.o
[3/211] Compiling C object lib/libevl.so.4.0.0.p/event.c.o
[4/211] Compiling C object lib/libevl.so.4.0.0.p/flags.c.o
[5/211] Compiling C object lib/libevl.so.4.0.0.p/heap.c.o
[6/211] Compiling C object lib/libevl.so.4.0.0.p/init.c.o
[7/211] Compiling C object lib/libevl.so.4.0.0.p/mutex.c.o
[8/211] Compiling C object lib/libevl.so.4.0.0.p/observable.c.o
[9/211] Compiling C object lib/libevl.so.4.0.0.p/parse_vdso.c.o
[10/211] Compiling C object lib/libevl.so.4.0.0.p/poll.c.o
[11/211] Compiling C object lib/libevl.so.4.0.0.p/proxy.c.o
[12/211] Compiling C object lib/libevl.so.4.0.0.p/rwlock.c.o
				...
[210/211] Installing files.
Installing lib/libevl.so.4.0.0 to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/lib
Installing lib/libevl.a to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/lib
Installing benchmarks/latmus to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/bin
Installing benchmarks/hectic to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/bin
Installing utils/evl to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/bin
Installing utils/evl-ps to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/libexec/evl
Installing utils/evl-check to /var/lab/nfsroot/homelab/armv7/5.x-xenomai4/usr/libexec/evl
				...
Running custom install script '/bin/sh /work/git/xenomai/v4/libevl/meson/post-install.sh'
```

#### Cross-compiling `libevl`

With the information available from the previous section in mind,
let's say the library source code is located at `~/git/libevl`, and
the kernel sources featuring the EVL core with the UAPI headers we
need is located at `~/git/linux-evl`. We want to build `libevl` so as
to target the ARMv7 architecture, specifically an i.MX6Q SoC.

Assuming we would compile using a [linaro ARM
toolchain](https://www.linaro.org/downloads/), we can use the
cross-file shipped with `libevl` named `'arm-linux-gnueabihf'`. In
this scenario, cross-compiling `libevl` and installing the resulting
library and utilities to a staging directory located at
`/nfsroot/imx6qp/usr/evl` would amount to this:

```
# Create and configure the build directory
$ mkdir /tmp/build-imx6q && cd /tmp/build-imx6q
$ meson setup --cross-file ~/git/libevl/meson/arm-linux-gnueabihf -Dbuildtype=release -Dprefix=/usr/evl -Duapi=~/git/linux-evl . ~/git/libevl

# Build the whole thing
$ meson compile

# Eventually, install the result
$ DESTDIR=/nfsroot/imx6qp ninja install
```

Done.

#### Native `libevl` build

Alternatively, you may want to build a native version of `libevl`
system, using the native toolchain from the build host.  Installing
the resulting library and utilities directly to their final home
located at e.g. `/opt/evl` is done as follows:

```

# Prepare the build directory
$ mkdir /tmp/build-native && cd /tmp/build-native
$ meson setup -Dbuildtype=release -Dprefix=/opt/evl -Duapi=~/git/linux-evl . ~/git/libevl

# Build it
$ meson compile

# Install the result
$ ninja install
```

Done too.

### Testing the installation

At this point, you really want to [test the EVL installation]({{<
relref "core/testing.md" >}}).

---

{{<lastmodified>}}
