---
menuTitle: "Ethernet"
title: "NIC drivers"
weight: 4
---

EVL implements a lightweight [network stack]({{< relref
"core/net/" >}}) which supports packet-oriented and UDP
protocols over Ethernet. Although EVL does not _require_ the NIC
driver code to be oob-capable, i.e. conveying ingress and egress
traffic directly from the [out-of-band execution stage]({{< relref
"dovetail/kernel-api/_index.md#two-stage-pipeline" >}}), having such
support in place _is the only way_ to have a complete, end-to-end
real-time networking path, from the Ethernet wire to the application
code. In other words, one may still use stock controller drivers along
with the EVL network stack, at the expense of the real-time
performance which would depend on the low-latency capabilities of the
host kernel.

This [section]({{< relref "dovetail/kernel-api/net/devices" >}}) details the
changes required for adding such out-of-band I/O capabilities to an
existing network interface controller driver from the stock Linux
kernel, using the Dovetail interface which extends the common
[networking support]({{< relref "dovetail/kernel-api/net" >}}). Although the EVL
network stack currently deals with protocols over an Ethernet medium,
these changes are medium-agnostic.

---

{{<lastmodified>}}
