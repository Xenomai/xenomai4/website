
# Legacy libevl build

{{% notice warning %}}
The information contained in this document only applies to `libevl`
prior to release #29 (r29), with a build system based on mere
Makefiles. These days, the [meson](https://mesonbuild.com) build
system is used instead, as described in this [document]({{< relref
"core/build-steps#building-libevl" >}}).
{{% /notice %}}

The generic command for building `libevl` prior to release 29 was:

```
$ make [-C $SRCDIR] [ARCH=$cpu_arch] [CROSS_COMPILE=$toolchain] UAPI=$uapi_dir [OTHER_BUILD_VARS] [goal...]
```

> Main build variables

| Variable   |  Description
| --------   |    -------
| $SRCDIR    |  Path to this source tree
| $cpu_arch  |   CPU architecture you build for ('arm', 'arm64', 'x86')
| $toolchain |  Optional prefix of the binutils filename (e.g. 'arm-linux-gnueabihf-', 'aarch64-linux-gnu-')

> Other build variables

| Variable      |  Description   |  Default
| --------      |    -------     |  -------
| D={0\|1}      |  Disable or enable debug build, i.e. -g -O0 vs -O2    | 0
| O=$output_dir |  Generate binary output files into $output_dir        | .
| V={0\|1}      |  Set build verbosity level, 0 is terse                | 0
| DESTDIR=$install_dir | Install library and binaries into $install_dir | /usr/evl

> Make goals

| Goal    |     Action
| ---     |     ---
| all     |     generate all binaries (library, utilities and tests)
| clean   |     remove the build files
| install |     do all, copying the generated system binaries to $DESTDIR in the process
| install_all | install, copying all the generated binaries including the tidbits

### Cross-compiling EVL

Let's say the library source code is located at ~/git/libevl, and the
kernel sources featuring the EVL core is located at
~/git/linux-evl.

Cross-compiling EVL and installing the resulting library and utilities
to a staging directory located at /nfsroot/\<machine\>/usr/evl would
amount to this:

> Cross-compiling from a separate build directory

```
# First create a build directory the where output files should go
$ mkdir /tmp/build-imx6q && cd /tmp/build-imx6q
# Then start the build+install process
$ make -C ~/git/libevl O=$PWD ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- UAPI=~/git/linux-evl DESTDIR=/nfsroot/imx6q/usr/evl install
```

or,

> Cross-compiling from the EVL library source tree

```
$ mkdir /tmp/build-hikey
$ cd ~/git/libevl
$ make O=/tmp/build-hikey ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- UAPI=~/git/linux-evl DESTDIR=/nfsroot/hikey/usr/evl install
```

{{% notice note %}}
This is good practice to always generate the build output files to a
separate build directory using the O= directive on the _make_ command
line, not to clutter your source tree with those. Generating output to
a separate directory also creates convenience Makefiles on the fly in
the output tree, which you can use to run subsequent builds without
having to mention the whole series of variables and options on the
_make_ command line again.
{{% /notice %}}

### Native EVL build

Conversely, you may want to build EVL natively on the target system.
Installing the resulting library and utilities directly to their final
home located at e.g. /usr/evl can be done as follows:

> Building natively from a build directory

```
$ mkdir /tmp/build-native && cd /tmp/build-native
$ make -C ~/git/libevl O=$PWD UAPI=~/git/linux-evl DESTDIR=/usr/evl install
```

or,

> Building natively from the EVL library source tree

```
$ mkdir /tmp/build-native
$ cd ~/git/libevl
$ make O=/tmp/build-native UAPI=~/git/linux-evl DESTDIR=/usr/evl install
```

---

{{<lastmodified>}}
