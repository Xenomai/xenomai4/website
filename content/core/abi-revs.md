---
title: ABI revisions
---

<div>
<style>
#abimap {
       width: 35%;
       margin-left: auto;
       margin-right: auto;
}
#abimap th {
       text-align: center;
}
#abimap td {
       text-align: center;
}
#abimap tr:nth-child(even) {
       background-color: #f2f2f2;
}
</style>

<table id="abimap">
  <col width="5%">
  <col width="90%">
  <col width="5%">
  <tr>
    <th>Revision</th>
    <th>Purpose</th> 
    <th>libevl release</th>
  </tr>
  <tr>
    <td>37</td>
    <td>Allow for eBPF-based filtering on early RX path</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r50" target="_blank">r50</a></td>
  </tr>
  <tr>
    <td>36</td>
    <td>Align on latest/final prctl() syscall form.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r49" target="_blank">r49</a></td>
  </tr>
  <tr>
    <td>35</td>
    <td>Add proxied polling operation.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r42" target="_blank">r42</a></td>
  </tr>
  <tr>
    <td>34</td>
    <td>Enable oob I/O interface on event mask.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r42" target="_blank">r42</a></td>
  </tr>
  <tr>
    <td>33</td>
    <td>Allow in-band threads to use event mask monitors (wait-any, unicast only).</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r42" target="_blank">r42</a></td>
  </tr>
  <tr>
    <td>32</td>
    <td>Extend the ungated event monitor interface (conjunctive/disjunctive wait on event mask, broadcasting).</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r42" target="_blank">r42</a></td>
  </tr>
  <tr>
    <td>31</td>
    <td>EVL_HMDIAG_LKDEPEND is now sent to the thread which switches in-band while owning mutexes (no more to the waiter(s) sleeping on such mutex(es)).</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r41" target="_blank">r41</a></td>
  </tr>
  <tr>
    <td>30</td>
    <td>Introduce the T_WOSO flag for enabling notification on schedule overrun.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r40" target="_blank">r40</a></td>
  </tr>
  <tr>
    <td>29</td>
    <td>Complete overhaul of the locking model to address basic ABBA issues in the mutex support code. This ABI release is marking a functional upgrade, not an interface change.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r39" target="_blank">r39</a></td>
  </tr>
  <tr>
    <td>28</td>
    <td>Report differentiated causes for interrupted wait on gated monitors from EVL_MONIOC_WAIT.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r37" target="_blank">r37</a></td>
  </tr>
  <tr>
    <td>27</td>
    <td>Handle prctl()-based syscall form. This enables Valgrind for EVL applications, while keeping backward compatibility for the legacy call form.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r28" target="_blank">r28</a></td>
  </tr>
  <tr>
    <td>26</td>
    <td>Add socket interface.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r26" target="_blank">r21</a></td>
  </tr>
  <tr>
    <td>25</td>
    <td>Add latmus request for measuring in-band response time to synthetic interrupt latency.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r21" target="_blank">r21</a></td>
  </tr>
  <tr>
    <td>24</td>
    <td>Add proxy read side.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r19" target="_blank">r19</a></td>
  </tr>
  <tr>
    <td>23</td>
    <td>Add the Observable element, and thread observability.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r17" target="_blank">r17</a></td>
  </tr>
  <tr>
    <td>22</td>
    <td>Add EVL_THRIOC_UNBLOCK, EVL_THRIOC_DEMOTE and EVL_THRIOC_YIELD, update EVL_THRIOC_*_MODE operations.</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r16" target="_blank">r16</a></td>
  </tr>
  <tr>
    <td>21</td>
    <td>Introduce element visibility attribute</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r15" target="_blank">r15</a></td>
  </tr>
  <tr>
    <td>20</td>
    <td>Add support for compat mode (32-bit exec over 64-bit kernel)</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r12" target="_blank">r12</a></td>
  </tr>
  <tr>
    <td>19</td>
    <td>Make y2038 safe</td>
    <td><a href="https://git.xenomai.org/xenomai4/libevl/-/tags/r11" target="_blank">r11</a></td>
  </tr>
  <tr>
    <td>18</td>
    <td>Plan for supporting a range of ABI revisions</td>
    <td>-</td>
  </tr>
  <tr>
    <td>17</td>
    <td>Replace SIGEVL_ACTION_HOME with RETUSER event</td>
    <td>-</td>
  </tr>
  <tr>
    <td>16</td>
    <td>Add synchronous breakpoint support</td>
    <td>-</td>
  </tr>
  <tr>
    <td>15</td>
    <td>Notify stax-related oob exclusion via SIGDEBUG_STAGE_LOCKED</td>
    <td>-</td>
  </tr>
  <tr>
    <td>14</td>
    <td>Add stax test helpers to 'hectic' driver</td>
    <td>-</td>
  </tr>
  <tr>
    <td>13</td>
    <td>Add stage exclusion lock mechanism</td>
    <td>-</td>
  </tr>
  <tr>
    <td>12</td>
    <td>Add support for recursive gate monitor lock</td>
    <td>-</td>
  </tr>
  <tr>
    <td>11</td>
    <td>Read count of timer expiries as a 64bit value</td>
    <td>-</td>
  </tr>
  <tr>
    <td>10</td>
    <td>Track count of remote thread wakeups</td>
    <td>-</td>
  </tr>
  <tr>
    <td>9</td>
    <td>Complete information returned by EVL_THRIOC_GET_STATE</td>
    <td>-</td>
  </tr>
  <tr>
    <td>8</td>
    <td>Add query for CPU state</td>
    <td>-</td>
  </tr>
  <tr>
    <td>7</td>
    <td>Drop time remainder return from EVL_CLKIOC_SLEEP</td>
    <td>-</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Enable fixed-size writes to proxy</td>
    <td>-</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Ensure latmus sends the ultimate bulk of results</td>
    <td>-</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Split per-thread debug mode flags</td>
    <td>-</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Add count of referrers to poll object shared state</td>
    <td>-</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Drop obsolete T_MOVED from thread status bits</td>
    <td>-</td>
  </tr>
  <tr>
    <td>1</td>
    <td>Add protocol specifier to monitor element</td>
    <td>-</td>
  </tr>
  <tr>
    <td>0</td>
    <td>Initial revision</td>
    <td>-</td>
  </tr>
</table>
</div>

---

{{<lastmodified>}}
