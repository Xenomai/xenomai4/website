---
title: "Kernel flag"
weight: 25
---

### Flag services

An EVL kernel flag is a basic synchronization mechanism based on the
EVL [wait queue]({{< relref "core/kernel-api/wait/" >}}), which can be
used to wait for a boolean state to be notified. This is typically
used in interrupt to thread synchronization, with the former posting a
wakeup event to the latter by mean of raising a flag.

{{< proto evl_init_flag >}}
void evl_init_flag(struct evl_flag *wf)
{{< /proto >}}

This call initializes a kernel flag.

{{% argument wf %}}
A flag descriptor is constructed by [evl_init_flag()]({{< relref
"#evl_init_flag" >}}), which contains ancillary information other
calls will need. `wf` is a pointer to such descriptor of type
`struct evl_flag`.
{{% /argument %}}

---

{{< proto evl_destroy_flag >}}
void evl_destroy_flag(struct evl_flag *wf)
{{< /proto >}}

This call destroys an existing EVL kernel flag. Any thread which might
blocked waiting on the flag is woken up by this call, receiving a
'resource removed' status (-EIDRM).

{{% argument wf %}}
The descriptor of the kernel flag to be destroyed.
{{% /argument %}}

---

{{< proto evl_wait_flag >}}
int evl_wait_flag(struct evl_flag *wf)
{{< /proto >}}

This service waits for the flag to be raised.  Until this happens, the
caller is blocked until a call to [evl_raise_flag()]({{< relref
"#evl_raise_flag" >}}) eventually releases it. If the flag was already
raised on entry, the caller returns immediately.  Waiters are queued
by order of [scheduling priority]({{< relref
"core/user-api/scheduling/_index.md" >}}).

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

Zero is returned on success, otherwise:

- -EIDRM	The flag was deleted while the caller was sleeping
  		on it. When this status is returned, the flag must
		be considered stale and should not be accessed anymore.

- -EINTR	The sleep was interrupted or forcibly unblocked.

---

{{< proto evl_wait_flag_timeout >}}
int evl_wait_flag_timeout(struct evl_flag *wf, ktime_t timeout, enum evl_tmode timeout_mode)
{{< /proto >}}

This call is a variant of [evl_wait_flag()]({{% relref
"#evl_wait_flag" %}}) which allows specifying a timeout on the
operation, so that the caller is automatically unblocked when a time
limit is reached.

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

{{% argument timeout %}}
A time limit for the call. There are two special values:
`EVL_NONBLOCK` tells the core NOT to block the caller if the flag
is not raised on entry, `EVL_INFINITE` means no time limit _only_ if
`timeout_mode` is `EVL_REL` (See [evl_wait_flag()]({{< relref
"#evl_wait_flag" >}})).
{{% /argument %}}

{{% argument timeout_mode %}}
A timeout mode, telling the core whether `timeout` should be
interpreted as a relative value representing a delay (`EVL_REL`), or
an absolute date (`EVL_ABS`).
{{% /argument %}}

Zero is returned on success, otherwise:

- -ETIMEDOUT	The timeout fired, after the amount of time specified by
	   	`timeout`.

- -EAGAIN	`EVL_NONBLOCK` was given in `timeout` but the flag
  		was not in a signaled state on entry.

- -EIDRM	The flag was deleted while the caller was sleeping
  		on it. When this status is returned, the flag must
		be considered stale and should not be accessed anymore.

- -EINTR	The sleep was interrupted or forcibly unblocked.

---

{{< proto evl_wait_flag_head >}}
struct evl_thread *evl_wait_flag_head(struct evl_flag *wf)
{{< /proto >}}

Returns the descriptor of the EVL [kernel thread]({{< relref
"core/kernel-api/kthread/" >}}) leading the wait queue.

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

{{% notice warning %}}
The flag must have been locked by a call to [evl_lock_flag()]({{<
relref "core/kernel-api/flag/#evl_lock_flag" >}}) prior to calling
this routine.
{{% /notice %}}

The thread descriptor returned by this call is guaranteed valid only
while such lock is held by the caller. A reference can be taken on
that thread via a call to `evl_get_element()` while the flag is
locked, in order to extend the guarantee until the last reference is
dropped by a call to `evl_put_element()`. In the meantime, the flag
may be unlocked safely, without the thread going stale.

```
int peek_at_flag(struct evl_flag *wf)
{
	struct evl_thread *t = NULL;
	unsigned long flags;

	evl_lock_flag(wf, flags);

	t = evl_wait_flag_head(wf);
	if (t)	/* Prevent 't' from going stale once the flag is unlocked. */
		evl_get_element(&t->element);

	evl_unlock_flag(wf, flags);

	if (!t)
		return -EAGAIN;

	do_stuff(t);

	evl_put_element(&t->element); /* Drop the reference. */

	return 0;
}
```

[evl_wait_flag_head()]({{< relref "#evl_wait_flag_head" >}}) returns a
pointer to the descriptor of the EVL thread currently leading the wait
queue, or NULL if none.

---

{{< proto evl_raise_flag_nosched >}}
void evl_raise_flag_nosched(struct evl_flag *wf)
{{< /proto >}}

[evl_raise_flag_nosched()]({{< relref "#evl_raise_flag_nosched" >}})
transitions the flag to the notified state, allowing all waiters to
compete for consuming the notification. The thread winning this race
resets the flag then unblocks from [evl_wait_flag()]({{< relref
"core/kernel-api/flag/#evl_wait_flag" >}}), others go back sleeping
until the next event is signaled. The [rescheduling procedure]({{<
relref "core/kernel-api/scheduling/#evl_schedule" >}}) is not called
by this routine, which makes it usable in sections of code guarded by
[spinlocks]({{< relref "core/kernel-api/spinlock/" >}}).

{{% notice info %}}
This call transitions the flag to a notified state until the event is
consumed by one of the waiters which is unblocked as a result.  If you
look for a pulse-mode notification which unblocks all threads waiting
for the next notification, you should refer to
[evl_pulse_flag_nosched()]({{< relref "core/kernel-api/flag/#evl_wait_flag"
>}}) instead.
{{% /notice %}}

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

{{% notice warning %}}
The caller MUST call [evl_schedule()]({{< relref
"core/kernel-api/scheduling/#evl_schedule" >}}) afterwards, as soon as
all locks have been dropped.
{{% /notice %}}

---

{{< proto evl_raise_flag >}}
void evl_raise_flag(struct evl_flag *wf)
{{< /proto >}}

[evl_raise_flag()]({{< relref "#evl_raise_flag" >}}) has the same
synchronization effect than [evl_raise_flag_nosched()]({{< relref
"#evl_raise_flag_nosched" >}}) but does invoke the [rescheduling
procedure]({{< relref "core/kernel-api/scheduling/#evl_schedule" >}})
implicitly in case a thread was resumed as a result of the operation.

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto evl_pulse_flag_nosched >}}
void evl_pulse_flag_nosched(struct evl_flag *wf)
{{< /proto >}}

[evl_pulse_flag_nosched()]({{< relref "#evl_pulse_flag_nosched" >}})
broadcasts a notification so that all waiters can consume the same
event, differing from [evl_raise_flag_nosched()]({{< relref
"#evl_raise_flag_nosched" >}}) which only allows a single waiter to do
so. This operation does not transition the flag to the notified state.
The [rescheduling procedure]({{< relref
"core/kernel-api/scheduling/#evl_schedule" >}}) is not called by this
routine, which makes it usable in sections of code guarded by
[spinlocks]({{< relref "core/kernel-api/spinlock/" >}}).

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto evl_pulse_flag >}}
void evl_pulse_flag(struct evl_flag *wf)
{{< /proto >}}

[evl_pulse_flag()]({{< relref "#evl_pulse_flag" >}}) has the same
synchronization effect than [evl_pulse_flag_nosched()]({{< relref
"#evl_pulse_flag_nosched" >}}) but does invoke the [rescheduling
procedure]({{< relref "core/kernel-api/scheduling/#evl_schedule" >}})
implicitly in case a thread was resumed as a result of the operation.

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto evl_flush_flag_nosched >}}
void evl_flush_flag_nosched(struct evl_flag *wf, int reason)
{{< /proto >}}

[evl_flush_flag_nosched()]({{< relref "#evl_flush_flag_nosched" >}})
is a variant of [evl_pulse_flag_nosched()]({{< relref
"#evl_pulse_flag_nosched" >}}) which allows the caller to specify the
wake up bitmask passed to the underlying [wait queue]({{<
relref "core/kernel-api/wait/" >}}), instead of [T_BCAST](https://source.denx.de/Xenomai/xenomai4/linux-evl/-/blob/v5.15.y-evl-rebase/include/uapi/evl/thread.h#L52).
This bitmask describes the reason for the wake up.

{{% notice warning %}}
This call is part of the inner EVL kernel API, any misusage can lead
to serious trouble. If you do not know about the internal wake up
logic, then you should refrain from using it.
{{% /notice %}}

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

{{% argument reason %}}
A bitmask which gives additional information to the resuming threads
about the reason why they were unblocked. In the common case, `reason`
should be zero. A non-zero value contains a flag bit matching a
[particular
situation](https://source.denx.de/Xenomai/xenomai4/linux-evl/-/blob/v5.15.y-evl-rebase/include/uapi/evl/thread.h#L41),
which translates to a specific error status for
[evl_wait_schedule()]({{< relref "core/kernel-api/wait/#evl_wait_schedule" >}}).
{{% /argument %}}

---

{{< proto evl_flush_flag >}}
void evl_flush_flag(struct evl_flag *wf, int reason)
{{< /proto >}}

[evl_flush_flag()]({{< relref "#evl_flush_flag" >}}) has the same
synchronization effect than [evl_flush_flag_nosched()]({{< relref
"#evl_flush_flag_nosched" >}}) but does invoke the [rescheduling
procedure]({{< relref "core/kernel-api/scheduling/#evl_schedule" >}})
implicitly in case a thread was resumed as a result of the operation.

---

{{< proto evl_clear_flag >}}
void evl_clear_flag(struct evl_flag *wf)
{{< /proto >}}

Reset the flag state to unnotified.

{{% argument wf %}}
The flag descriptor constructed by either [evl_init_flag()]({{<
relref "#evl_init_flag" >}}), or statically built with
[EVL_FLAG_INITIALIZER]({{% relref "#EVL_FLAG_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto EVL_FLAG_INITIALIZER >}}
EVL_FLAG_INITIALIZER(name)
{{< /proto >}}

A macro which expands as a static initializer you can use in a C
statement creating an EVL kernel flag.

{{% argument name %}}
The C variable name to which the initializer should be assigned.
{{% /argument %}}

```
struct evl_flag foo = EVL_FLAG_INITIALIZER(foo);
```

---

{{< proto DEFINE_EVL_FLAG >}}
DEFINE_EVL_FLAG(name)
{{< /proto >}}

A macro which expands as a C statement defining an initialized EVL
kernel flag.

{{% argument name %}}
The C variable name of the kernel flag to define.
{{% /argument %}}

```
/*
 * The following expands as:
 * static struct evl_flag bar = EVL_FLAG_INITIALIZER(bar);
 */
static DEFINE_EVL_FLAG(bar);
```

---

{{<lastmodified>}}
