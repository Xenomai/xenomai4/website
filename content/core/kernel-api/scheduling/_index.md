---
menutitle: "Scheduling"
weight: 33
---

EVL defines [five scheduling policies]({{< relref
"core/user-api/scheduling/" >}}) for running out-of-band threads in
user space. Only two of them are available to EVL [kernel threads]({{<
relref "core/kernel-api/kthread/" >}}), namely:

- [SCHED_FIFO]({{< relref "core/user-api/scheduling/#SCHED_FIFO" >}}), which is the common
  first-in, first-out real-time policy.

- [SCHED_WEAK]({{< relref "core/user-api/scheduling/#SCHED_WEAK" >}}),
  which is a *non real-time* policy.

The scheduling policy of an EVL kernel thread is defined at creation
time by the priority argument passed to the [evl_run_kthread()]({{<
relref "core/kernel-api/kthread/#evl_run_kthread" >}}) call.

## Scheduler services {#sched-services}

{{< proto evl_schedule >}}
void evl_schedule()
{{< /proto >}}

This is the rescheduling procedure for kernel space code, which is the
equivalent of the `schedule()` from the in-band kernel. When and where
to call it follows this rule of thumb:

- [evl_schedule()]({{< relref "#evl_schedule" >}}) MUST be called to
  apply the changes to the scheduler state caused by any primitive
  service which wakes up a thread, such as [evl_wake_up()]({{< relref
  "core/kernel-api/wait/#evl_wake_up" >}}), or EVL services suffixed
  by `_nosched` which explicitly refrain from calling the rescheduling
  procedure, such as [evl_raise_flag_nosched()]({{< relref
  "core/kernel-api/flag/#evl_raise_flag_nosched" >}}).

- there is NO need to call [evl_schedule()]({{< relref "#evl_schedule"
  >}}) after any EVL routine which is known to block the caller.

- a code path MUST NOT call [evl_schedule()]({{< relref
  "#evl_schedule" >}}) while holding a spinlock.
