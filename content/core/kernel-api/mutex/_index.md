---
title: "Kernel mutex"
weight: 10
---

EVL kernel mutexes are useful for serializing EVL threads from kernel
space, typically some EVL driver code would use them. User _and_
kernel EVL threads can be serialized by EVL kernel mutexes
indifferently. An EVL kernel mutex applies a priority inheritance
protocol upon contention.

### Kernel mutex services

{{< proto evl_init_kmutex >}}
void evl_init_kmutex(struct evl_kmutex *mutex)
{{< /proto >}}

This call initializes a kernel mutex which can be used to serialize
EVL threads running on the out-of-band context.

{{% argument mutex %}}
A mutex descriptor is constructed by [evl_init_kmutex()]({{< relref
"#evl_init_kmutex" >}}), which contains ancillary information other
calls will need. _mutex_ is a pointer to such descriptor of type
`struct evl_kmutex`.
{{% /argument %}}

---

{{< proto evl_destroy_kmutex >}}
void evl_destroy_kmutex(struct evl_kmutex *mutex)
{{< /proto >}}

This call destroys an existing EVL kernel mutex. Any thread which
might blocked waiting on the mutex is woken up by this call, receiving
a 'resource removed' status (-EIDRM).

{{% argument mutex %}}
The descriptor of the kernel mutex to be destroyed.
{{% /argument %}}

---

{{< proto evl_trylock_kmutex >}}
int evl_trylock_kmutex(struct evl_kmutex *kmutex)
{{< /proto >}}

[evl_trylock_kmutex()]({{< relref "#evl_trylock_kmutex" >}}) attempts to
lock a kernel mutex, returning immediately on success or failure
without blocking the caller.

{{% argument mutex %}}
The descriptor of the kernel mutex to lock.
{{% /argument %}}

This call returns zero if the kernel mutex was acquired and locked
successfully by the caller. Otherwise, a negated error code is
returned:

- -EBUSY is returned if the mutex is currently locked by another
   thread.

- -EDEADLOCK is returned if a deadlock condition was detected while
   attempting to lock the mutex.

---

{{< proto evl_lock_kmutex >}}
int evl_lock_kmutex(struct evl_kmutex *kmutex)
{{< /proto >}}

[evl_lock_kmutex()]({{< relref "#evl_lock_kmutex" >}}) locks a kernel
mutex. If the lock is owned by another thread on entry, the caller is
blocked until the access is granted. If multiple threads wait for
acquiring the lock, the one with the [highest scheduling priority]({{<
relref "core/user-api/scheduling" >}}) which has been waiting for the
longest time is served first.

{{% argument mutex %}}
The descriptor of the kernel mutex to lock.
{{% /argument %}}

This call returns zero if the kernel mutex was acquired and locked
successfully by the caller. Otherwise, a negated error code is
returned:

- -EIDRM indicates that the mutex was deleted while the caller was
  sleeping on it. When this status is returned, the mutex must be
  considered stale and should not be accessed anymore.

- -EDEADLOCK is returned if a deadlock condition was detected while
   attempting to lock the mutex.

- -EOWNERDEAD is returned if the mutex was locked on entry, but the
   current owner disappeared from the system, leading to an
   inconsistent state. This error cannot be fixed, the resource
   protected by this mutex may be in some undefined state. In other
   words, such error is definitely bad news.

{{% notice note %}}
Acquiring an EVL kernel mutex must be done from the out-of-band
context exclusively. In addition, as long as the caller holds an EVL
mutex, switching to in-band mode is wrong since this would introduce a
priority inversion. Unlike from the user-space context, there is no
guard preventing from doing so in kernel space though.
{{% /notice %}}

---

{{< proto evl_unlock_kmutex >}}
int evl_unlock_kmutex(struct evl_kmutex *kmutex)
{{< /proto >}}

[evl_unlock_kmutex()]({{< relref "#evl_unlock_kmutex" >}}) unlocks a
kernel mutex previously acquired by a call to [evl_lock_kmutex()]({{<
relref "#evl_lock_kmutex" >}}). The thread leading the wait queue for
the mutex is transferred ownership of the released mutex atomically if
any.

{{% argument mutex %}}
The descriptor of the kernel mutex to unlock.
{{% /argument %}}

{{% notice note %}}
Only the thread which acquired an EVL kernel mutex may release it.
{{% /notice %}}

---

{{< proto EVL_KMUTEX_INITIALIZER >}}
EVL_KMUTEX_INITIALIZER(name)
{{< /proto >}}

A macro which expands as a static initializer you can use in a
C statement creating an EVL kernel mutex.

{{% argument name %}}
The C variable name to which the initializer should be assigned.
{{% /argument %}}

```
struct evl_kmutex foo = EVL_KMUTEX_INITIALIZER(foo);
```

---

{{< proto DEFINE_EVL_KMUTEX >}}
DEFINE_EVL_KMUTEX(name)
{{< /proto >}}

A macro which expands as a C statement defining an initialized EVL
kernel mutex.

{{% argument name %}}
The C variable name of the kernel mutex to define.
{{% /argument %}}

```
/*
 * The following expands as:
 * static struct evl_kmutex bar = EVL_KMUTEX_INITIALIZER(bar);
 */
static DEFINE_EVL_KMUTEX(bar);
```

---

{{<lastmodified>}}
