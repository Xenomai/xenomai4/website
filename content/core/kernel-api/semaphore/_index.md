---
title: "Kernel semaphore"
weight: 20
---

The EVL core provides a counting semaphore API in kernel space, which
drivers can use to synchronize threads, with a requirement for waiters
to be EVL threads running on the out-of-band stage. However, [a
semaphore]({{< relref "#evl_up" >}}) may be posted by any kind of
thread (EVL or regular), running on any stage at the time of the call.

### Semaphore services

{{< proto evl_init_ksem >}}
void evl_init_ksem(struct evl_ksem *ksem, unsigned int value)
{{< /proto >}}

This call initializes a kernel semaphore.

{{% argument ksem %}}
A semaphore descriptor is constructed by [evl_init_ksem()]({{< relref
"#evl_init_ksem" >}}), which contains ancillary information other
calls will need. `ksem` is a pointer to such descriptor of type
`struct evl_ksem`.
{{% /argument %}}

{{% argument value %}}
The initial value of the semaphore.
{{% /argument %}}

---

{{< proto evl_destroy_ksem >}}
void evl_destroy_ksem(struct evl_ksem *ksem)
{{< /proto >}}

This call destroys an existing EVL kernel semaphore. Any thread which
might be waiting on the semaphore is woken up by this call, receiving
a 'resource removed' status (-EIDRM).

{{% argument ksem %}}
The descriptor of the kernel semaphore to be destroyed.
{{% /argument %}}

---

{{< proto evl_down >}}
int evl_down(struct evl_ksem *ksem)
{{< /proto >}}

This service decrements the semaphore by one.  If the resulting
semaphore value is negative, the caller is blocked until a call to
[evl_up()]({{< relref "#evl_up" >}}) eventually releases
it. Otherwise, the caller returns immediately.  Waiters are queued by
order of [scheduling priority]({{< relref
"core/user-api/scheduling/_index.md" >}}).

{{% argument ksem %}}
The semaphore descriptor constructed by either [evl_init_ksem()]({{<
relref "#evl_init_ksem" >}}), or statically built with
[EVL_KSEM_INITIALIZER]({{% relref "#EVL_KSEM_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto evl_down_timeout >}}
int evl_down_timeout(struct evl_ksem *ksem, ktime_t timeout)
{{< /proto >}}

This call is a variant of [evl_down()]({{% relref "#evl_down"
%}}) which allows specifying a timeout on the down operation, so that
the caller is automatically unblocked when a time limit is reached.

{{% argument ksem %}}
The semaphore descriptor constructed by either [evl_init_ksem()]({{<
relref "#evl_init_ksem" >}}), or statically built with
[EVL_KSEM_INITIALIZER]({{% relref "#EVL_KSEM_INITIALIZER" %}}).
{{% /argument %}}

{{% argument timeout %}}
A time limit for the call, which is implicitly based on the
monotonic clock.
{{% /argument %}}

Zero is returned on success, otherwise:

- -ETIMEDOUT	The timeout fired, after the amount of time specified by
	   	`timeout`.

- -EIDRM	The semaphore was deleted while the caller was sleeping
  		on it. When this status is returned, the semaphore must
		be considered stale and should not be accessed anymore.

---

{{< proto evl_trydown >}}
int evl_trydown(struct evl_ksem *ksem)
{{< /proto >}}

This call attempts to decrement the semaphore provided the result does
not yields a negative count. Otherwise, the routine immediately
returns with an error status.

Zero is returned on success, otherwise:

-EAGAIN       the semaphore value is zero or negative at the time of the call.

---

{{< proto evl_up >}}
void evl_up(struct evl_ksem *ksem)
{{< /proto >}}

This call posts a semaphore by incrementing its count by one. If a
thread is sleeping on the semaphore as a result of a previous call to
[evl_down()]({{% relref "#evl_down" %}}) or [evl_down_timeout()]({{%
relref "#evl_down_timeout" %}}), the thread heading the wait queue is
unblocked.

{{% argument ksem %}}
The semaphore descriptor constructed by either [evl_init_ksem()]({{<
relref "#evl_init_ksem" >}}), or statically built with
[EVL_KSEM_INITIALIZER]({{% relref "#EVL_KSEM_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto evl_broadcast >}}
void evl_broadcast(struct evl_ksem *ksem)
{{< /proto >}}

This call unblocks all the threads blocked on the semaphore at the
time of the call. Every unblocked thread receives a success status.

{{% argument ksem %}}
The semaphore descriptor constructed by either [evl_init_ksem()]({{<
relref "#evl_init_ksem" >}}), or statically built with
[EVL_KSEM_INITIALIZER]({{% relref "#EVL_KSEM_INITIALIZER" %}}).
{{% /argument %}}

---

{{< proto EVL_KSEM_INITIALIZER >}}
EVL_KSEM_INITIALIZER(name)
{{< /proto >}}

A macro which expands as a static initializer you can use in a
C statement creating an EVL kernel semaphore.

{{% argument name %}}
The C variable name to which the initializer should be assigned.
{{% /argument %}}

```
struct evl_ksem foo = EVL_KSEM_INITIALIZER(foo);
```

---

{{< proto DEFINE_EVL_KSEM >}}
DEFINE_EVL_KSEM(name)
{{< /proto >}}

A macro which expands as a C statement defining an initialized EVL
kernel semaphore.

{{% argument name %}}
The C variable name of the kernel semaphore to define.
{{% /argument %}}

```
/*
 * The following expands as:
 * static struct evl_ksem bar = EVL_KSEM_INITIALIZER(bar);
 */
static DEFINE_EVL_KSEM(bar);
```

---

{{<lastmodified>}}
